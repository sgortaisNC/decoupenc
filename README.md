**Général**
-
- Installer un compilateur SCSS
- Modifier Bootstrap au lieu de surcharger
- Utiliser PHPStorm pour la découpe 
- Liste de CDN pour les librairies js : https://cdnjs.com/libraries
- HTML des animations : animations.html ... ouais !

**Compatibilité**
-
**Navigateur**
- IE 11+
- FF 42+
- Chrome 46+
- Edge
- Safari 9+ (iOs)
- Opéra 32+

**Devices**
- PC
- Tablette portait
- Tablette paysage
- Smartphone portait (320px de large)
- Et oui, iPhone aussi petit malin ! 


**CSS**
- 
- " !important " - interdit !
- Utilisez la syntaxe SaSS au maximum.
- Pas de Z-index 99999999999
- Le flex c'est bien, mais attention au erreur avec IE : https://caniuse.com/#feat=flexbox
- Si un élément Bootstrap ne correspond pas exactement au besoin, modifiez le plutôt que de le recréer from scratch
- Gardez en tête que le CSS de la home devra être **FACILEMENT** réutilisable sur les autres pages

**Amélioration**
-
- Méthodologie BEM : http://getbem.com/introduction/
