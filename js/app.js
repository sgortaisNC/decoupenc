jQuery(document).ready(function ($) {

    /*** UTILITIES ***/
    // wait function
    $.wait = function (ms) {
        var defer = $.Deferred();
        setTimeout(function () {
            defer.resolve();
        }, ms);
        return defer;
    };

    //ScrollTo
    function scrollTo(target, condition) {
        if (condition.length) {
            var scroll = target.offset().top - $('header').height(); // prise en compte du header sticky - si sticky partiel, mettre la partie du header qui sera sticky à la place
            $("html, body").stop().animate({scrollTop: scroll}, 1500);
        }
    }

    /*** Fin UTILITIES ***/

    /*** GLOBAL ***/
    // Tooltips trigger
    $("[data-toggle='tooltip']").tooltip();

    // tableau responsive
    $('.wysiwyg table, .main-content table').wrap('<div class="tableau-responsive container">');

    // loader sur les boutons
    $('.btn').on('click', function () {
        if (!$(this).hasClass('no-loader')) {
            $(this).width($(this).width()); // pour empêcher le changement de taille du bouton
            $(this).addClass('btn-loader');
            $(this).html('<i class="fas fa-circle-notch fa-spin"></i>');
        }
        // ajoutez la class "no-loader" aux boutons que vous souhaitez exclure
    });

    // loader sur le bouton submit de gravity forms
    $('.gform_footer .gform_button').on('click', function () {
        $(this).parent().addClass('gform-btn-loader');
        $(this).parent().append('<div class="msg-envoi"><i class="fas fa-circle-notch fa-spin"></i><span>Envoi du formulaire en cours...</span></div>');
    });

    // Scroll to error - gravity forms
    scrollTo($(".formulaire-anchor"), $(".gform_validation_error")); // ajoutez la classe "formulaire-anchor" dans les réglages du formulaire en back-office

    // Tarte au citron contraignant
    setTimeout(function () {
        if ($("#tarteaucitronAlertBig").css("display") === "block") {
            $("#tarteaucitronRoot").addClass("blur");
        }
    }, 1500);
    $(document).on("click", "#tarteaucitronPersonalize", function () {
        $("#tarteaucitronRoot").removeClass("blur");
    });
    $(document).on("click", "#tarteaucitronClosePanel", function () {
        window.location.reload();
    });


    /*** FIN GLOBAL ***/

    /*** STICKY HEADER ***/
    // var position_header = $('.conteneur-subheader').offset().top;
    // $(window).on('scroll', function(){
    //     if($(document).scrollTop() > position_header){
    //         $('header').addClass('sticky');
    //     } else {
    //         $('header').removeClass('sticky');
    //     }
    //
    // });
    /*** FIN STICKY HEADER ***/


    /*** MEGAMENU ***/
    // Affichage du dropdown au clic sur le titre de niveau 1 correspondant
    $('.titre-niveau-1').on('click', function () {
        if ($(window).width() > 991) { // uniquement en desktop
            if($(this).siblings('.dropdown').hasClass('active')){
                $('.dropdown').removeClass('active');
            } else {
                $('.dropdown').removeClass('active'); // on cache tous les dropdowns...
                $(this).siblings('.dropdown').slideDown().addClass('active'); // sauf celui qui nous intéresse
            }
        }
    });

    // fermeture de tous les dropdowns quand on clique à l'extérieur du menu
    $(document).click(function (event) {
        if (!$(event.target).closest('.megamenu').length) {
            if ($(window).width() > 991) { // uniquement en desktop
                $('.dropdown').removeClass('active');
            }
        }
    });
    /*** FIN MEGAMENU ***/

    /*** MENU BURGER ***/
    $('.hamburger').on('click', function () {
        /* reset du menu */
        $('.dropdown').removeClass('active');
        $('.dropdown-niveau-2').removeClass('active');
        $('.controls').removeClass('active');
        /* reset du menu */

        $(this).toggleClass('is-active'); // toggle l'animation sur le picto burger
        $('header').toggleClass('menu-opened'); // toggle le changement du header quand le menu est ouvert
        $('.menu').toggleClass('active'); // toggle l'ouverture/fermeture du menu
        $('body').toggleClass('no-scroll'); // empêche le scroll sur la page lorsque le menu est ouvert

        // sous-niveaux
        $('.titre-niveau-1').on('click', function () {
            $(this).siblings('.dropdown').addClass('active'); // accéder aux niveaux 2
            $('.controls').addClass('active');
        });

        $('.btn-dropdown').on('click', function () {
            $(this).siblings('.dropdown-niveau-2').addClass('active'); // accéder aux niveaux 3
        });

        // bouton retour
        $('.controls .close').on('click', function () {
            if ($('.dropdown-niveau-2').hasClass('active')) { // sur la liste des niveaux 3
                $('.dropdown-niveau-2').removeClass('active');
            } else { // sur la liste des niveaux 2
                $('.dropdown').removeClass('active');
                $('.controls').removeClass('active');
            }
        });

    });
    /*** FIN MENU BURGER ***/

    /*** SLICKS ****/
    // mettez ici tous les slicks du site

    // slider home
    $('.slider-home').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false
    });

    /*** FIN SLICKS ***/

});